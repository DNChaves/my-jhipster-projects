# README #

Sample App that implements a borrowed objects control. Built via JHipster, a generator that can be used to construct an Java app with Angular, Spring Boot and some other components. More info at (jhipster.github.io or http://www.jhipster.tech/).
For this sample App, I used an monolithic architecture, with Angular (and Bootstrap 4 for better styling), Spring Boot and a persistence layer on PostgreSQL. Spring Data for querying. All being build on Maven.
### What is this repository for? ###

* App emprestimoApp developed By Daniel N. Chaves for studying pourpouses on JHipster, Angular, Spring Boot, etc...
* 1.0

### How do I get set up? ###

* Download code and, with maven on path, run ./ mvnw
* Postgres installed on default port (5432). To see more information on how to change database config, please go to www.jhipster.tech or ask me, and I am glad to tell you.

### Who do I talk to? ###

* Repo owner and admin: Daniel Nogueira Chaves