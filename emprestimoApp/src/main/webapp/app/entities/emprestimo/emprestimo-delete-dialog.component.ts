import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Emprestimo } from './emprestimo.model';
import { EmprestimoPopupService } from './emprestimo-popup.service';
import { EmprestimoService } from './emprestimo.service';

@Component({
    selector: 'jhi-emprestimo-delete-dialog',
    templateUrl: './emprestimo-delete-dialog.component.html'
})
export class EmprestimoDeleteDialogComponent {

    emprestimo: Emprestimo;

    constructor(
        private emprestimoService: EmprestimoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.emprestimoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'emprestimoListModification',
                content: 'Deleted an emprestimo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-emprestimo-delete-popup',
    template: ''
})
export class EmprestimoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private emprestimoPopupService: EmprestimoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.emprestimoPopupService
                .open(EmprestimoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
