import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { Emprestimo } from './emprestimo.model';
import { EmprestimoService } from './emprestimo.service';

@Component({
    selector: 'jhi-emprestimo-detail',
    templateUrl: './emprestimo-detail.component.html'
})
export class EmprestimoDetailComponent implements OnInit, OnDestroy {

    emprestimo: Emprestimo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private emprestimoService: EmprestimoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEmprestimos();
    }

    load(id) {
        this.emprestimoService.find(id).subscribe((emprestimo) => {
            this.emprestimo = emprestimo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEmprestimos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'emprestimoListModification',
            (response) => this.load(this.emprestimo.id)
        );
    }
}
