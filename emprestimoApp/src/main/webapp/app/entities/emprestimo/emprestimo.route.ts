import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { EmprestimoComponent } from './emprestimo.component';
import { EmprestimoDetailComponent } from './emprestimo-detail.component';
import { EmprestimoPopupComponent } from './emprestimo-dialog.component';
import { EmprestimoDeletePopupComponent } from './emprestimo-delete-dialog.component';

export const emprestimoRoute: Routes = [
    {
        path: 'emprestimo',
        component: EmprestimoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'emprestimoApp.emprestimo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'emprestimo/:id',
        component: EmprestimoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'emprestimoApp.emprestimo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const emprestimoPopupRoute: Routes = [
    {
        path: 'emprestimo-new',
        component: EmprestimoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'emprestimoApp.emprestimo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'emprestimo/:id/edit',
        component: EmprestimoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'emprestimoApp.emprestimo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'emprestimo/:id/delete',
        component: EmprestimoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'emprestimoApp.emprestimo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
