import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Emprestimo } from './emprestimo.model';
import { EmprestimoService } from './emprestimo.service';

@Injectable()
export class EmprestimoPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private emprestimoService: EmprestimoService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.emprestimoService.find(id).subscribe((emprestimo) => {
                    if (emprestimo.dataEmprestimo) {
                        emprestimo.dataEmprestimo = {
                            year: emprestimo.dataEmprestimo.getFullYear(),
                            month: emprestimo.dataEmprestimo.getMonth() + 1,
                            day: emprestimo.dataEmprestimo.getDate()
                        };
                    }
                    this.ngbModalRef = this.emprestimoModalRef(component, emprestimo);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.emprestimoModalRef(component, new Emprestimo());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    emprestimoModalRef(component: Component, emprestimo: Emprestimo): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.emprestimo = emprestimo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
