import { BaseEntity } from './../../shared';

export class Emprestimo implements BaseEntity {
    constructor(
        public id?: number,
        public observacoes?: string,
        public devolvido?: boolean,
        public dataEmprestimo?: any,
        public pessoa?: BaseEntity,
        public item?: BaseEntity,
    ) {
        this.devolvido = false;
    }
}
