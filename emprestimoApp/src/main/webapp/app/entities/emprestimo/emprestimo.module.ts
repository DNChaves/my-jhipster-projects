import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EmprestimoAppSharedModule } from '../../shared';
import {
    EmprestimoService,
    EmprestimoPopupService,
    EmprestimoComponent,
    EmprestimoDetailComponent,
    EmprestimoDialogComponent,
    EmprestimoPopupComponent,
    EmprestimoDeletePopupComponent,
    EmprestimoDeleteDialogComponent,
    emprestimoRoute,
    emprestimoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...emprestimoRoute,
    ...emprestimoPopupRoute,
];

@NgModule({
    imports: [
        EmprestimoAppSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EmprestimoComponent,
        EmprestimoDetailComponent,
        EmprestimoDialogComponent,
        EmprestimoDeleteDialogComponent,
        EmprestimoPopupComponent,
        EmprestimoDeletePopupComponent,
    ],
    entryComponents: [
        EmprestimoComponent,
        EmprestimoDialogComponent,
        EmprestimoPopupComponent,
        EmprestimoDeleteDialogComponent,
        EmprestimoDeletePopupComponent,
    ],
    providers: [
        EmprestimoService,
        EmprestimoPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EmprestimoAppEmprestimoModule {}
