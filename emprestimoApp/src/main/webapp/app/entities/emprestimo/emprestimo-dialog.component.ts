import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Emprestimo } from './emprestimo.model';
import { EmprestimoPopupService } from './emprestimo-popup.service';
import { EmprestimoService } from './emprestimo.service';
import { Pessoa, PessoaService } from '../pessoa';
import { Item, ItemService } from '../item';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-emprestimo-dialog',
    templateUrl: './emprestimo-dialog.component.html'
})
export class EmprestimoDialogComponent implements OnInit {

    emprestimo: Emprestimo;
    isSaving: boolean;

    pessoas: Pessoa[];

    items: Item[];
    dataEmprestimoDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private emprestimoService: EmprestimoService,
        private pessoaService: PessoaService,
        private itemService: ItemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.pessoaService.query()
            .subscribe((res: ResponseWrapper) => { this.pessoas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.itemService.query()
            .subscribe((res: ResponseWrapper) => { this.items = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.emprestimo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.emprestimoService.update(this.emprestimo));
        } else {
            this.subscribeToSaveResponse(
                this.emprestimoService.create(this.emprestimo));
        }
    }

    private subscribeToSaveResponse(result: Observable<Emprestimo>) {
        result.subscribe((res: Emprestimo) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Emprestimo) {
        this.eventManager.broadcast({ name: 'emprestimoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackPessoaById(index: number, item: Pessoa) {
        return item.id;
    }

    trackItemById(index: number, item: Item) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-emprestimo-popup',
    template: ''
})
export class EmprestimoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private emprestimoPopupService: EmprestimoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.emprestimoPopupService
                    .open(EmprestimoDialogComponent as Component, params['id']);
            } else {
                this.emprestimoPopupService
                    .open(EmprestimoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
