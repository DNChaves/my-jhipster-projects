import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { EmprestimoAppItemModule } from './item/item.module';
import { EmprestimoAppPessoaModule } from './pessoa/pessoa.module';
import { EmprestimoAppEmprestimoModule } from './emprestimo/emprestimo.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        EmprestimoAppItemModule,
        EmprestimoAppPessoaModule,
        EmprestimoAppEmprestimoModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EmprestimoAppEntityModule {}
