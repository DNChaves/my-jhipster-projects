import { Component, OnInit } from '@angular/core';
import { LembreteService } from './lembrete.service';
import { JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiLanguageService, JhiAlertService } from 'ng-jhipster';
import { ResponseWrapper, Principal } from '../../shared';
import { Emprestimo } from '../../entities/emprestimo';
import { Subscription } from 'rxjs/Rx';

@Component({
  selector: 'jhi-lembrete',
  templateUrl: './lembrete.component.html',
  styles: [],
  providers: [LembreteService]
})
export class LembreteComponent implements OnInit {

  lembretes: Emprestimo[] = [];
  eventSubscriber: Subscription;

    constructor(private principal: Principal,
      private eventManager: JhiEventManager,
      private lembreteService: LembreteService,
      private alertService: JhiAlertService
  ) {}

  loadAll() {
    this.lembreteService.query({
    }).subscribe(
        (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
        (res: ResponseWrapper) => this.onError(res.json)
    );
  }

  private onSuccess(data, headers) {
    this.lembretes = [];
    for (let i = 0; i < data.length; i++) {
        this.lembretes.push(data[i]);
    }
  }

  private onError(error) {
    this.alertService.error(error.message, null, null);
  }

  ngOnInit() {
    console.log('OnInit called!');
    this.loadAll();
    this.registerAuthenticationSuccess();
  }

  registerAuthenticationSuccess() {
    this.eventManager.subscribe('authenticationSuccess', () => {
      this.loadAll();
    });
  }

  registerChangeInEmprestimos() {
    this.eventSubscriber = this.eventManager.subscribe('emprestimoListModification', () => this.loadAll());
  }

  isAuthenticated() {
    return this.principal.isAuthenticated();
  }
}
