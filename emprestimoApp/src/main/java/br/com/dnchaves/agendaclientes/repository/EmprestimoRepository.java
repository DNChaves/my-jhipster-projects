package br.com.dnchaves.agendaclientes.repository;

import br.com.dnchaves.agendaclientes.domain.Emprestimo;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;


/**
 * Spring Data JPA repository for the Emprestimo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmprestimoRepository extends JpaRepository<Emprestimo,Long> {
    
    public List<Emprestimo> findByDataEmprestimoBeforeAndDevolvidoFalse(LocalDate daysBefore);
}
