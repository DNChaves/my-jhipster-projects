/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.dnchaves.agendaclientes.web.rest.vm;
