package br.com.dnchaves.agendaclientes.service;

import java.util.List;

import br.com.dnchaves.agendaclientes.domain.Emprestimo;

public interface LembreteService {

    public List<Emprestimo> listMoreThen15DaysLate();

}
