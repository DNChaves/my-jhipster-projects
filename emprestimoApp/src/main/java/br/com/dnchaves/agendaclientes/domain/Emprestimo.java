package br.com.dnchaves.agendaclientes.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Emprestimo.
 */
@Entity
@Table(name = "emprestimo")
public class Emprestimo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "observacoes")
    private String observacoes;

    @Column(name = "devolvido")
    private Boolean devolvido;

    @NotNull
    @Column(name = "data_emprestimo", nullable = false)
    private LocalDate dataEmprestimo;

    @ManyToOne
    private Pessoa pessoa;

    @ManyToOne
    private Item item;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public Emprestimo observacoes(String observacoes) {
        this.observacoes = observacoes;
        return this;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Boolean isDevolvido() {
        return devolvido;
    }

    public Emprestimo devolvido(Boolean devolvido) {
        this.devolvido = devolvido;
        return this;
    }

    public void setDevolvido(Boolean devolvido) {
        this.devolvido = devolvido;
    }

    public LocalDate getDataEmprestimo() {
        return dataEmprestimo;
    }

    public Emprestimo dataEmprestimo(LocalDate dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
        return this;
    }

    public void setDataEmprestimo(LocalDate dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public Emprestimo pessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
        return this;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Item getItem() {
        return item;
    }

    public Emprestimo item(Item item) {
        this.item = item;
        return this;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Emprestimo emprestimo = (Emprestimo) o;
        if (emprestimo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), emprestimo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Emprestimo{" +
            "id=" + getId() +
            ", observacoes='" + getObservacoes() + "'" +
            ", devolvido='" + isDevolvido() + "'" +
            ", dataEmprestimo='" + getDataEmprestimo() + "'" +
            "}";
    }
}
