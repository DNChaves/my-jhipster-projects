package br.com.dnchaves.agendaclientes.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Pessoa.
 */
@Entity
@Table(name = "pessoa")
public class Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "contato", nullable = false)
    private String contato;

    @OneToMany(mappedBy = "pessoa")
    @JsonIgnore
    private Set<Emprestimo> emprestimoLists = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Pessoa nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getContato() {
        return contato;
    }

    public Pessoa contato(String contato) {
        this.contato = contato;
        return this;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public Set<Emprestimo> getEmprestimoLists() {
        return emprestimoLists;
    }

    public Pessoa emprestimoLists(Set<Emprestimo> emprestimos) {
        this.emprestimoLists = emprestimos;
        return this;
    }

    public Pessoa addEmprestimoList(Emprestimo emprestimo) {
        this.emprestimoLists.add(emprestimo);
        emprestimo.setPessoa(this);
        return this;
    }

    public Pessoa removeEmprestimoList(Emprestimo emprestimo) {
        this.emprestimoLists.remove(emprestimo);
        emprestimo.setPessoa(null);
        return this;
    }

    public void setEmprestimoLists(Set<Emprestimo> emprestimos) {
        this.emprestimoLists = emprestimos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pessoa pessoa = (Pessoa) o;
        if (pessoa.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pessoa.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pessoa{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", contato='" + getContato() + "'" +
            "}";
    }
}
