package br.com.dnchaves.agendaclientes.service.impl;

import br.com.dnchaves.agendaclientes.domain.Emprestimo;
import br.com.dnchaves.agendaclientes.repository.EmprestimoRepository;
import br.com.dnchaves.agendaclientes.service.LembreteService;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LembreteServiceImpl implements LembreteService {

    private final Logger log = LoggerFactory.getLogger(LembreteServiceImpl.class);

    private final EmprestimoRepository emprestimoRepository;

    public LembreteServiceImpl(EmprestimoRepository emprestimoRepository) {
        this.emprestimoRepository = emprestimoRepository;
    }

    public List<Emprestimo> listMoreThen15DaysLate() {
        List<Emprestimo> result = null;
        LocalDate fifteenDaysBefore = LocalDate.now();
        fifteenDaysBefore = fifteenDaysBefore.minusDays(15);
        result = emprestimoRepository.findByDataEmprestimoBeforeAndDevolvidoFalse(fifteenDaysBefore);           
        return result;
    }
}
