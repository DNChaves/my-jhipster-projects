package br.com.dnchaves.agendaclientes.repository;

import br.com.dnchaves.agendaclientes.domain.Item;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Item entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ItemRepository extends JpaRepository<Item,Long> {
    
}
