package br.com.dnchaves.agendaclientes.repository;

import br.com.dnchaves.agendaclientes.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
