package br.com.dnchaves.agendaclientes.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.dnchaves.agendaclientes.domain.Emprestimo;

import br.com.dnchaves.agendaclientes.repository.EmprestimoRepository;
import br.com.dnchaves.agendaclientes.service.LembreteService;
import br.com.dnchaves.agendaclientes.web.rest.util.HeaderUtil;
import br.com.dnchaves.agendaclientes.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Emprestimo.
 */
@RestController
@RequestMapping("/api")
public class LembreteResource {

    private final Logger log = LoggerFactory.getLogger(LembreteService.class);

    private final LembreteService lembreteService;

    public LembreteResource(LembreteService lembreteService) {
        this.lembreteService = lembreteService;
    }

    /**
     * GET  /lembretes : get all the lembretes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of lembretes in body
     */
    @GetMapping("/lembretes")
    @Timed
    public ResponseEntity<List<Emprestimo>> getEmprestimosEmAtraso() {
        log.debug("REST request to get a page of Emprestimos");

        return Optional.ofNullable(lembreteService.listMoreThen15DaysLate())
        .map(result -> new ResponseEntity<>(result, HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));       
    }

}
