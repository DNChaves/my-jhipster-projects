/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { EmprestimoAppTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { EmprestimoDetailComponent } from '../../../../../../main/webapp/app/entities/emprestimo/emprestimo-detail.component';
import { EmprestimoService } from '../../../../../../main/webapp/app/entities/emprestimo/emprestimo.service';
import { Emprestimo } from '../../../../../../main/webapp/app/entities/emprestimo/emprestimo.model';

describe('Component Tests', () => {

    describe('Emprestimo Management Detail Component', () => {
        let comp: EmprestimoDetailComponent;
        let fixture: ComponentFixture<EmprestimoDetailComponent>;
        let service: EmprestimoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [EmprestimoAppTestModule],
                declarations: [EmprestimoDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    EmprestimoService,
                    JhiEventManager
                ]
            }).overrideTemplate(EmprestimoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EmprestimoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EmprestimoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Emprestimo(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.emprestimo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
